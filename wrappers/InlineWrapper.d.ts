import * as React from 'react';
import { PopoverProps as PopoverPropsType } from '@mui/material/Popover';
import { WrapperProps } from './Wrapper';
import { TextFieldProps } from '@mui/material/TextField';
export declare const useStyles: (props?: any) => Record<"popoverPaper" | "popoverPaperWider", string>;
export interface InlineWrapperProps<T = TextFieldProps> extends WrapperProps<T> {
    /** Popover props passed to material-ui Popover (with variant="inline") */
    PopoverProps?: Partial<PopoverPropsType>;
}
export declare const InlineWrapper: React.FC<InlineWrapperProps>;
