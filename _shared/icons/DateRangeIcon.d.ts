import React from 'react';
import { SvgIconProps } from '@mui/material/SvgIcon';
export declare const DateRangeIcon: React.SFC<SvgIconProps>;
